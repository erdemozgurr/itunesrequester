//
//  MusicianArray.swift
//  iTunesRequester
//
//  Created by Mac on 12.12.2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

final class MusicianArray: Codable {
    
    var results: [MusicianTest]
    var resultsCount: Int?
    
    init(results: [MusicianTest]) {
        self.results = results
    }
    
    
    
}
