//
//  ViewController.swift
//  iTunesRequester
//
//  Created by Mac on 12.12.2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Kingfisher


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    var dataFilter: Int = 0 {
        didSet{
            
        }
    }
    var count: Int = 0 {
        didSet{
            
        }
    }
    
    
    var musicians: [MusicianArray] = []
    var filteredMusicians : [MusicianArray] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchTextField.delegate = self
        searchTextField.layer.masksToBounds = true
        searchTextField.layer.cornerRadius = 6
        searchTextField.layer.borderWidth = 1
        tableView.tableFooterView = UIView()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(sendAlertForSelectingMusicTypes))
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {


    }
    
    


}
extension ViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if musicians.count == 0 {
            return 0
        } else if filteredMusicians.count >= 1{
            return musicians[0].results.count
        } else if musicians[0].results.count >= 1
        {
            return musicians[0].results.count
        }
        //print(musicians[0].results.count)
        return 0
 
        
 
        

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MusicianTableViewCell
        cell!.title.text = "NOT FOUND YOUR WANTS GENRE"
        cell!.subTitle.text = "IT IS NOT YOUR WANTS GENRE OR TRACK NAME NOT FOUND"


        

        
        switch dataFilter {
        case 0:
            //cell?.isHidden = false
            guard musicians[0].results[indexPath.row].collectionName != nil else {return cell!}
            guard musicians[0].results[indexPath.row].trackName != nil else {return cell!}
            cell!.title.text = musicians[0].results[indexPath.row].trackName!
            cell!.subTitle.text = "\(musicians[0].results[indexPath.row].artistName!) -  \(musicians[0].results[indexPath.row].collectionName!)"

            
            cell?.imageView?.kf.indicatorType = .activity
            //cell?.imageView?.kf.setImage(with: URL(string: "\(self.musicians[0].results[indexPath.row].artworkUrl100!)"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
            //print(self.musicians[0].results[indexPath.row].artworkUrl30!)
            var frame = cell?.imageView?.frame
            frame?.size.height = 60 as CGFloat
            frame?.size.width = 60 as CGFloat
            cell?.imageView?.frame = frame!
            cell!.imageView!.kf.setImage(with: URL(string: "\(self.musicians[0].results[indexPath.row].artworkUrl60!)"))
            return cell!
        case 1:
            //cell!.isHidden = false
             guard musicians[0].results[indexPath.row].primaryGenreName == "Pop" else {return cell!}
            
             let newArray = musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!}
            
            filteredMusicians = newArray
             /*
            print(filteredMusicians[0].results[indexPath.row].primaryGenreName!)
            print(filteredMusicians[0].results[indexPath.row].trackName!)
            print(filteredMusicians[0].results[indexPath.row])
            */
             for filtered in filteredMusicians {
                print(filtered.results[indexPath.row].trackName!)
             }
             count = filteredMusicians[0].results.count
             print(count)

            
            //print(newArray[0].results[indexPath.row].primaryGenreName!)
            //count = newArray.count
            
            

             guard musicians[0].results[indexPath.row].collectionName != nil else {return cell!}
             guard musicians[0].results[indexPath.row].trackName != nil else {return cell!}
            //print(musicians[0].results[indexPath.row].primaryGenreName!)
             cell!.title.text = musicians[0].results[indexPath.row].trackName!
             cell!.subTitle.text = "(POP)\(musicians[0].results[indexPath.row].artistName!) -  \(musicians[0].results[indexPath.row].collectionName! )"

            return cell!

            case 2:
                //cell!.isHidden = false
                
                guard musicians[0].results[indexPath.row].collectionName != nil else {return cell!}
                guard musicians[0].results[indexPath.row].trackName != nil else {return cell!}
                //musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!}
                //print(musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!})
                guard musicians[0].results[indexPath.row].primaryGenreName == "Hip-Hop/Rap" else {return cell!}
                
                let newArray = musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Hip-Hop/Rap"))!}
                
                filteredMusicians = newArray
                /*
                 print(filteredMusicians[0].results[indexPath.row].primaryGenreName!)
                 print(filteredMusicians[0].results[indexPath.row].trackName!)
                 print(filteredMusicians[0].results[indexPath.row])
                 */
                for filtered in filteredMusicians {
                    print(filtered.results[indexPath.row])
                }

                
                
                //print(newArray[0].results[indexPath.row].primaryGenreName!)
                //count = newArray.count
                
                guard musicians[0].results[indexPath.row].collectionName != nil else {return cell!}
                guard musicians[0].results[indexPath.row].trackName != nil else {return cell!}
                //guard filteredMusicians[0].results[indexPath.row].collectionName != nil else {return cell!}
                //guard filteredMusicians[0].results[indexPath.row].trackName != nil else {return cell!}
                //print(musicians[0].results[indexPath.row].primaryGenreName!)
                cell!.title.text = musicians[0].results[indexPath.row].trackName!
                cell!.subTitle.text = "(Hip-Hop/Rap)\(musicians[0].results[indexPath.row].artistName!) -  \(musicians[0].results[indexPath.row].collectionName!) "

            
                return cell!
        case 3:
            //cell!.isHidden = false
            
            
            //musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!}
            //print(musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!})
            guard musicians[0].results[indexPath.row].primaryGenreName == "R&B/Soul" else {return cell!}
            
            let newArray = musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("R&B/Soul"))!}
            
            filteredMusicians = newArray
            /*
             print(filteredMusicians[0].results[indexPath.row].primaryGenreName!)
             print(filteredMusicians[0].results[indexPath.row].trackName!)
             print(filteredMusicians[0].results[indexPath.row])
             */
            for filtered in filteredMusicians {
                print(filtered.results[indexPath.row])
            }
            
            
            
            //print(newArray[0].results[indexPath.row].primaryGenreName!)
            //count = newArray.count
            
            guard musicians[0].results[indexPath.row].collectionName != nil else {return cell!}
            guard musicians[0].results[indexPath.row].trackName != nil else {return cell!}
            //guard filteredMusicians[0].results[indexPath.row].collectionName != nil else {return cell!}
            //guard filteredMusicians[0].results[indexPath.row].trackName != nil else {return cell!}
            //print(musicians[0].results[indexPath.row].primaryGenreName!)
            cell!.title.text = musicians[0].results[indexPath.row].trackName!
            cell!.subTitle.text = "(R&B/Soul)\(musicians[0].results[indexPath.row].artistName!) -  \(musicians[0].results[indexPath.row].collectionName!) "
            return cell!

        case 4:
            //cell!.isHidden = false
            
            
            //musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!}
            //print(musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Pop"))!})
            guard musicians[0].results[indexPath.row].primaryGenreName == "Comedy" else {return cell!}
            
            let newArray = musicians.filter{($0.results[indexPath.row].primaryGenreName?.contains("Comedy"))!}
            
            filteredMusicians = newArray
            /*
             print(filteredMusicians[0].results[indexPath.row].primaryGenreName!)
             print(filteredMusicians[0].results[indexPath.row].trackName!)
             print(filteredMusicians[0].results[indexPath.row])
             */
            for filtered in filteredMusicians {
                print(filtered.results[indexPath.row])
            }
            
            
            
            //print(newArray[0].results[indexPath.row].primaryGenreName!)
            //count = newArray.count
            
            
            guard musicians[0].results[indexPath.row].collectionName != nil else {return cell!}
            guard musicians[0].results[indexPath.row].trackName != nil else {return cell!}
            //guard filteredMusicians[0].results[indexPath.row].collectionName != nil else {return cell!}
            //guard filteredMusicians[0].results[indexPath.row].trackName != nil else {return cell!}
            //print(musicians[0].results[indexPath.row].primaryGenreName!)
            cell!.title.text = musicians[0].results[indexPath.row].trackName!
            cell!.subTitle.text = "(Comedy)\(musicians[0].results[indexPath.row].artistName!) -  \(musicians[0].results[indexPath.row].collectionName!) "
            return cell!
            
        default:
           
            return cell!
       
            
        }
        
        

        return cell!
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchTextField.resignFirstResponder()
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let lowerText = searchTextField.text?.lowercased()
        let aString = "search?term=\(lowerText!)"
        let searchedMusicianRequest = ResourceRequest<MusicianArray>(resourcePath: "\(aString)")
        print(searchedMusicianRequest)
        searchedMusicianRequest.getAll { [weak self] musicianResult in
            switch musicianResult{
            case .failure:
                print("failed")
            case .success(let musicians):
                DispatchQueue.main.async { [weak self] in
                   // guard let self = self else {return}
                    self!.musicians = [musicians]
                    self?.tableView.reloadData()
                    self?.dataFilter = 0
                    
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tableView.reloadData()
            
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "musicianDetailsViewController") as? MusicianDetailsViewController
        vc?.tmpArtistName = musicians[0].results[indexPath.row].artistName!
        vc?.tmpReleasedDate = musicians[0].results[indexPath.row].releaseDate!
        vc?.tmpCollectionName = musicians[0].results[indexPath.row].collectionName!
        vc?.tmpMusicianAlbumImageView = musicians[0].results[indexPath.row].artworkUrl100!
        vc?.tmpSongsCountInAlbum = musicians[0].results[indexPath.row].trackCount!
        vc?.tmpTrackTuru = musicians[0].results[indexPath.row].primaryGenreName!
        vc?.tmpPrice = musicians[0].results[indexPath.row].collectionPrice!
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func reload() {
        DispatchQueue.main.async() {
            self.tableView.reloadData()
        }
    }
    @objc func sendAlertForSelectingMusicTypes(){
        let alert = UIAlertController(title: "Music Genres", message: "Select the music genre what you want of the musician", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "All Songs", style: .default, handler: {
            action in
            self.dataFilter = 0
            self.tableView.reloadData()
            print(self.dataFilter)
            
        }))
        alert.addAction(UIAlertAction(title: "Pop", style: .default, handler: {
            action in
            self.dataFilter = 1
            self.tableView.reloadData()
            print(self.dataFilter)
        }))
        alert.addAction(UIAlertAction(title: "Hip-Hop/Rap", style: .default, handler: {
            action in
            self.dataFilter = 2
            self.tableView.reloadData()
            print(self.dataFilter)
        }))
        alert.addAction(UIAlertAction(title: "R&B/Soul", style: .default, handler: {
            action in
            self.dataFilter = 3
            self.tableView.reloadData()
            print(self.dataFilter)
        }))
        alert.addAction(UIAlertAction(title: "Comedy", style: .default, handler: {
            action in
            self.dataFilter = 4
            self.tableView.reloadData()
            print(self.dataFilter)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))


        

        
        
        self.present(alert, animated: true)
        
    }
    
    
}


