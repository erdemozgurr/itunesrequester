//
//  MusicianDetailsViewController.swift
//  iTunesRequester
//
//  Created by Mac on 12.12.2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class MusicianDetailsViewController: UIViewController {

    @IBOutlet weak var ustView: UIView!
    @IBOutlet weak var musicianAlbumImageView: UIImageView!
    @IBOutlet weak var collectionName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var trackTuru: UILabel!
    @IBOutlet weak var songsCountInAlbum: UILabel!
    @IBOutlet weak var releasedDate: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
    
    var tmpMusicianAlbumImageView = ""
    var tmpCollectionName = ""
    var tmpArtistName = ""
    var tmpTrackTuru = ""
    var tmpSongsCountInAlbum = 0
    var tmpReleasedDate = ""
    var tmpPrice = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionName.text = tmpCollectionName
        artistName.text = tmpArtistName
        trackTuru.text = tmpTrackTuru
        songsCountInAlbum.text =   "\(tmpSongsCountInAlbum)"
        releasedDate.text = tmpReleasedDate
        price.text = "\(tmpPrice)"
        musicianAlbumImageView.kf.setImage(with: URL(string: "\(tmpMusicianAlbumImageView)"))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
