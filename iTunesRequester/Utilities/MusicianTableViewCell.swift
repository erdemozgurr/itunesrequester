//
//  MusicianTableViewCell.swift
//  iTunesRequester
//
//  Created by Mac on 12.12.2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class MusicianTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
