import Foundation

enum GetResourcesRequest<ResourceType>{
    case success(ResourceType)
    case failure
}

struct ResourceRequest<ResourceType>
    where ResourceType: Codable{
    
    var url = URL(string: "http://itunes.apple.com/")
    
    let baseMusicianURL = "http://itunes.apple.com/"
    let resourceMusicianURL: URL
    
    init(resourcePath: String) {
        self.url = url.flatMap{URL(string: $0.description + resourcePath)}
        guard let resourceMusicianURL = URL(string: baseMusicianURL) else {fatalError()}
        self.resourceMusicianURL = resourceMusicianURL.appendingPathComponent(resourcePath)
    }
    
    func getAll(completion: @escaping(GetResourcesRequest<ResourceType>) -> Void){
        
        let dataTask = URLSession.shared
            .dataTask(with: url!) { (data, _, _) in
                guard let jsonData  = data else {
                    completion(.failure)
                    return
                }
                do {
                    let resources = try JSONDecoder().decode(ResourceType.self, from: jsonData)
                    completion(.success(resources))
                }
                catch{
                    completion(.failure)
                }
        }
        dataTask.resume()
    }
    
    
    
}
