import Foundation

final class Musician: Codable {
    
    var wrapperType: String
    var kind: String
    var artistId: Int?
    var collectionId: Int
    var trackId: Int
    var artistName: String
    var collectionName: String
    var trackName: String
    var collectionCensoredName: String
    var trackCensoredName: String
    var artistViewUrl: String
    var collectionViewUrl: String
    var trackViewUrl: String
    var previewUrl: String
    var artworkUrl30: String
    var artworkUrl60: String
    var artworkUrl100: String
    var collectionPrice: Double
    var trackPrice: Double
    var releaseDate: String
    var collectionExplicitness: String
    var trackExplicitness: String
    var discCount: Int
    var discNumber: Int
    var trackCount: Int
    var trackNumber: Int
    var trackTimeMillis: Int
    var country: String
    var currency: String
    var primaryGenreName: String
    var isStreamable: Bool
    
    init(wrapperType: String, kind: String, artistId: Int, collectionId: Int, trackId: Int, artistName: String, collectionName: String, trackName: String, collectionCensoredName: String, trackCensoredName: String, artistViewUrl: String, collectionViewUrl: String, trackViewUrl: String, previewUrl: String, artworkUrl30: String, artworkUrl60: String, artworkUrl100: String, collectionPrice: Double, trackPrice: Double, releaseDate: String, collectionExplicitness: String, trackExplicitness: String, discCount: Int, discNumber: Int, trackCount: Int, trackNumber: Int, trackTimeMillis: Int, country: String, currency: String, primaryGenreName: String, isStreamable: Bool) {
        self.wrapperType = wrapperType
        self.kind = kind
        self.artistId = artistId
        self.collectionId = collectionId
        self.trackId = trackId
        self.artistName = artistName
        self.collectionName = collectionName
        self.trackName = trackName
        self.collectionCensoredName = collectionCensoredName
        self.trackCensoredName = trackCensoredName
        self.artistViewUrl = artistViewUrl
        self.collectionViewUrl = collectionViewUrl
        self.trackViewUrl = trackViewUrl
        self.previewUrl = previewUrl
        self.artworkUrl60 = artworkUrl60
        self.artworkUrl100 = artworkUrl100
        self.collectionPrice = collectionPrice
        self.trackPrice = trackPrice
        self.collectionExplicitness = collectionExplicitness
        self.trackExplicitness = trackExplicitness
        self.discCount = discCount
        self.discNumber = discNumber
        self.trackCount = trackCount
        self.trackNumber = trackNumber
        self.trackTimeMillis = trackTimeMillis
        self.country = country
        self.currency = currency
        self.primaryGenreName = primaryGenreName
        self.artworkUrl30 = artworkUrl30
        self.isStreamable = isStreamable
        self.releaseDate = releaseDate
    }
    
    
}
